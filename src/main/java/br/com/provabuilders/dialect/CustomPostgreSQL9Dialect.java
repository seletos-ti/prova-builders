package br.com.provabuilders.dialect;

import org.hibernate.dialect.PostgreSQL9Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

public class CustomPostgreSQL9Dialect extends PostgreSQL9Dialect {

	public CustomPostgreSQL9Dialect() {
		super();
		registerFunction("DATE_ADD", new SQLFunctionTemplate(StandardBasicTypes.TIMESTAMP, "(CAST((?1) AS TIMESTAMP) + CAST((?2) || (?3) AS INTERVAL))"));
		registerFunction("DATE_SUB", new SQLFunctionTemplate(StandardBasicTypes.TIMESTAMP, "(CAST((?1) AS TIMESTAMP) + CAST((?2) || (?3) AS INTERVAL))"));
	}

}