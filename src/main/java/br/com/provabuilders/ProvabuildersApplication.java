package br.com.provabuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvabuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvabuildersApplication.class, args);
	}

}
