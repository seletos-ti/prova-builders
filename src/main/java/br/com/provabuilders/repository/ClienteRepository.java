package br.com.provabuilders.repository;

import br.com.provabuilders.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClienteRepository extends SuperRepository<Cliente, Long> {

    @Query("FROM Cliente c " +
            "WHERE LOWER(c.nome) like %:searchTerm% " +
            "OR LOWER(c.cpf) like %:searchTerm%")
    Page<Cliente> search(
            @Param("searchTerm") String searchTerm,
            Pageable pageable);
}
