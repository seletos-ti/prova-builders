package br.com.provabuilders.service.impl;

import br.com.provabuilders.entity.Cliente;
import br.com.provabuilders.repository.ClienteRepository;
import br.com.provabuilders.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente save(Cliente Cliente) {
        return clienteRepository.save(Cliente);
    }

    @Override
    public void delete(Long id) {
        clienteRepository.deleteById(id);
    }

    @Override
    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    @Override
    public Cliente getOne(Long id) {
        return clienteRepository.getOne(id);
    }

    @Override
    public Page<Cliente> search(
            String searchTerm,
            int page,
            int size) {
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "nome");
        return clienteRepository.search(
                searchTerm.toLowerCase(),
                pageRequest);
    }
}