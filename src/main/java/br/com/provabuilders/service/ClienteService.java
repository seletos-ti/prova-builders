package br.com.provabuilders.service;

import br.com.provabuilders.entity.Cliente;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ClienteService {

    Cliente save(Cliente cliente);

    void delete(Long id);

    List<Cliente> findAll();

    Cliente getOne(Long id);

    Page<Cliente> search(
            String searchTerm,
            int page,
            int size);
}
