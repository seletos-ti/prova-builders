package br.com.provabuilders.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@SequenceGenerator(name = Cliente.SEQUENCE_NAME, sequenceName = Cliente.SEQUENCE_NAME, allocationSize = 1)
public class Cliente extends SuperEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    static final String SEQUENCE_NAME = "CLIENTE_SEQUENCE_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private Long id;

    private String nome;

    private String cpf;

    private Date dataNascimento;

    @Transient
    private int idade;

    public int getIdade() {
        if (this.dataNascimento != null) {
            LocalDate start = Instant.ofEpochMilli(this.dataNascimento.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate stop = LocalDate.now();
            long years = java.time.temporal.ChronoUnit.YEARS.between( start , stop );
            return (int) years;
        }
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}
