package br.com.provabuilders.controller;

import br.com.provabuilders.entity.Cliente;
import br.com.provabuilders.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public Cliente save(@RequestBody @Validated Cliente person) {
        return clienteService.save(person);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        clienteService.delete(id);
    }

    @GetMapping("{id}")
    public Cliente getOne(@PathVariable Long id) {
        return clienteService.getOne(id);
    }

    @GetMapping()
    public List<Cliente> findAll() {
        return clienteService.findAll();
    }

    @GetMapping("/search")
    public Page<Cliente> search(
            @RequestParam("searchTerm") String searchTerm,
            @RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size) {
        return clienteService.search(searchTerm, page, size);

    }
}
